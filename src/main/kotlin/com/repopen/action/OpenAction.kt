package com.repopen.action

import com.intellij.openapi.actionSystem.AnAction
import com.intellij.openapi.actionSystem.AnActionEvent

import java.awt.Desktop
import java.net.URI

class OpenAction : AnAction() {

    override fun actionPerformed(e: AnActionEvent) {
        if (Desktop.isDesktopSupported() && Desktop.getDesktop().isSupported(Desktop.Action.BROWSE)) {
            Desktop.getDesktop().browse( URI("http://www.gitlab.com/"));
        }
    }
}